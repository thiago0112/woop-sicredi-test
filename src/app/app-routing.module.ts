import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './authentication/login/login.component';
import { DashboardComponent } from './dragons/dashboard/dashboard.component';
import { AuthGuard } from './authentication/auth.guard';
import { DetailsComponent } from './dragons/details/details.component';
import { FormComponent } from './dragons/form/form.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/dragons/list',
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'dragons/list',
    canActivate: [AuthGuard],
    component: DashboardComponent
  },
  {
    path: 'dragons/details/:id',
    canActivate: [AuthGuard],
    component: DetailsComponent
  },
  {
    path: 'dragons/create',
    canActivate: [AuthGuard],
    component: FormComponent
  },
  {
    path: 'dragons/edit/:id',
    canActivate: [AuthGuard],
    component: FormComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
