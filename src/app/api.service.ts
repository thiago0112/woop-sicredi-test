import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  /**
   * Api base url
   * @string
   */
  apiURL: string = environment.apiUrl;

  constructor(public httpClient: HttpClient) {}
}
