import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { LoginComponent } from './login/login.component';
import { AuthGuard } from './auth.guard';
import { CustomMaterialModule } from '../custom-material.module';
import { AuthService } from './auth.service';

@NgModule({
  declarations: [ LoginComponent ],
  imports: [
    CommonModule,
    FormsModule,
    CustomMaterialModule,
    ReactiveFormsModule
  ],
  exports: [LoginComponent],
  providers: [AuthGuard, AuthService]
})
export class AuthenticationModule {}
