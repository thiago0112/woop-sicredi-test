import { Injectable } from '@angular/core';
import { environment } from './../../environments/environment';
import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  /**
   * Authentication state
   */
  authenticated: boolean;

  /**
   * Logged in user
   */
  user: User;

  /**
   * Login attempt
   * @param user
   */
  login(user: User) {
    if (user.username === environment.demo_username
      && user.password === environment.demo_password
    ) {
      this._setSession(user);
      return true;
    }

    return false;
  }

  /**
   * Set authentication session
   * @param user User
   */
  private _setSession(user: User) {
    this.user = user;
    this.authenticated = true;
  }
}
