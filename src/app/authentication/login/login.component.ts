import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { User } from '../user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  loginForm = this.fb.group({
    username: [null, Validators.required],
    password: [null, Validators.required],
  });

  invalidCredentials = false;

  constructor(
    private authService: AuthService,
    private router: Router,
    private fb: FormBuilder
  ) { }

  onSubmit() {
    if (!this.loginForm.valid) {
      return;
    }

    const user = this.loginForm.value as User;

    if (!this.authService.login(user)) {
      this.invalidCredentials = true;
      return;
    }

    this.invalidCredentials = false;
    this.router.navigate(['']);
  }
}
