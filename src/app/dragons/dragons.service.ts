import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';
import { Dragon } from './dragon';

/**
 * Dragons api service
 */
@Injectable({
  providedIn: 'root'
})
export class DragonsService extends ApiService {
  /**
   * Get list of dragons
   * @return Observable<Dragon[]>
   */
  getList() {
    return this.httpClient.get<Dragon[]>(`${this.apiURL}/dragon`);
  }

  /**
   * Get dragon details
   * @param id number
   * @return Observable<Dragon>
   */
  getDetails(id: number) {
    return this.httpClient.get<Dragon>(`${this.apiURL}/dragon/${id}`);
  }

  /**
   * Create dragon
   * @param dragon Dragon
   * @return Observable<Dragon>
   */
  create(dragon: Dragon) {
    return this.httpClient.post<Dragon[]>(`${this.apiURL}/dragon`, dragon);
  }

  /**
   * Update dragon
   * @param id number
   * @param dragon Dragon
   * @return Observable<Dragon>
   */
  update(id: number, dragon: Dragon) {
    return this.httpClient.put<Dragon[]>(`${this.apiURL}/dragon/${id}`, dragon);
  }

  /**
   * Remove dragon
   * @param dragon Dragon
   * @return Observable<Dragon>
   */
  delete(dragon: Dragon) {
    return this.httpClient.delete(`${this.apiURL}/dragon/${dragon.id}`);
  }
}
