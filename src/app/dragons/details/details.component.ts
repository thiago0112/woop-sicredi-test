import { Component, OnInit } from '@angular/core';
import { DragonsService } from '../dragons.service';
import { ActivatedRoute } from '@angular/router';
import { Dragon } from '../dragon';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {
  /**
   * Dragon data
   */
  dragon: Dragon;

  constructor(
    private dragonsService: DragonsService,
    private route: ActivatedRoute
  ) {}

  /**
   * Load dragon details
   */
  ngOnInit(): void {
    const id = parseInt(this.route.snapshot.paramMap.get('id'), 0);

    this.dragonsService
      .getDetails(id)
      .subscribe(dragon => this.dragon = dragon);
  }
}
