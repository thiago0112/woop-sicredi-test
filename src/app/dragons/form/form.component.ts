import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  Validators,
  FormArray,
  FormControl
} from '@angular/forms';
import { DragonsService } from '../dragons.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ENTER, COMMA, SPACE } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material';
import { Dragon } from '../dragon';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  dragonForm = this.fb.group({
    name: [null, Validators.required],
    type: [null, Validators.required],
    histories: this.fb.array([])
  });
  /**
   * Dragon identifier
   */
  id: number;

  /**
   * Form type
   */
  creation = true;

  /**
   * Chip separator key codes
   */
  separatorKeysCodes = [ENTER, COMMA, SPACE];

  constructor(
    private fb: FormBuilder,
    private dragonsService: DragonsService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  /**
   * Get form type and load data
   */
  ngOnInit(): void {
    this.id = parseInt(this.route.snapshot.paramMap.get('id'), 0);

    if (!this.id) {
      return;
    }

    this.creation = false;
    this.getSavedData();
  }

  /**
   * Save dorm
   */
  onSubmit(): void {
    if (! this.dragonForm.valid) {
      return;
    }

    this.save().subscribe(() => this.router.navigate(['']));
  }

  /**
   * Add history chip
   * @param event MatChipInputEvent
   */
  addHistory(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      const histories = this.dragonForm.get('histories') as FormArray;
      histories.push(this.fb.control(value.trim()));
    }

    if (input) {
      input.value = '';
    }
  }

  /**
   * Remove history chip
   *
   * @param index number
   */
  removeHistory(index: number): void {
    const histories = this.dragonForm.get('histories') as FormArray;

    if (index >= 0) {
      histories.removeAt(index);
    }
  }

  /**
   * Switch saving method
   */
  private save(): Observable<any> {
    const dragon = this.dragonForm.value as Dragon;

    if (this.creation) {
      return this.dragonsService.create(dragon);
    } else {
      return this.dragonsService.update(this.id, dragon);
    }
  }

  /**
   * Get saved form data
   * @return void
   */
  private getSavedData(): void {
    this.dragonsService.getDetails(this.id).subscribe(dragon => {
      Object.keys(dragon).forEach(key => {
        const control = this.dragonForm.get(key);
        const value = dragon[key];
        if (control instanceof FormControl) {
          control.setValue(value);
        }

        if (control instanceof FormArray && value instanceof Array) {
          value.forEach(valueItem => control.push(this.fb.control(valueItem)));
        }
      });
    });
  }
}
