export class Dragon {
  id: number;
  createdAt: string;
  type: string;
  name: string;
  histories: string[];
}
