import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CustomMaterialModule } from '../custom-material.module';
import { UtilsModule } from '../utils/utils.module';
import { RouterModule } from '@angular/router';
import { DetailsComponent } from './details/details.component';
import { FormComponent } from './form/form.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    DashboardComponent,
    DetailsComponent,
    FormComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    CustomMaterialModule,
    UtilsModule,
    ReactiveFormsModule
  ],
  exports: [
    DashboardComponent,
    DetailsComponent,
    FormComponent
  ]
})
export class DragonsModule { }
