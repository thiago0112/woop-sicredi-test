import { Component, OnInit, ViewChild } from '@angular/core';
import { DragonsService } from '../dragons.service';
import { Dragon } from '../dragon';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  dataSource: Dragon[];

  displayedColumns = ['id', 'name', 'actions'];

  constructor(private dragonsService: DragonsService){}

  /**
   * Load data on init
   */
  ngOnInit(): void {
    this.getDragonsList();
  }

  /**
   * Get dragons list
   */
  getDragonsList(): void {
    this.dragonsService.getList().subscribe(dataSource => {
      this.dataSource = dataSource.sort((left, right) => {
        return left.name > right.name ? 1 : left.name < right.name ? -1 : 0;
      });
    });
  }

  /**
   * Remove dragon
   * @param dragon Dragon
   */
  delete(dragon: Dragon): void {
    this.dragonsService.delete(dragon).subscribe(() => this.getDragonsList());
  }
}
